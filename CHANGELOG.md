
## 0.5.12 [10-15-2024]

* Changes made at 2024.10.14_20:38PM

See merge request itentialopensource/adapters/adapter-juniper_mist!21

---

## 0.5.11 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-juniper_mist!19

---

## 0.5.10 [08-14-2024]

* Changes made at 2024.08.14_18:53PM

See merge request itentialopensource/adapters/adapter-juniper_mist!18

---

## 0.5.9 [08-07-2024]

* Changes made at 2024.08.06_20:06PM

See merge request itentialopensource/adapters/adapter-juniper_mist!17

---

## 0.5.8 [07-17-2024]

* fix dependency vulnerability

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!16

---

## 0.5.7 [03-28-2024]

* Changes made at 2024.03.28_13:20PM

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!14

---

## 0.5.6 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!12

---

## 0.5.5 [03-13-2024]

* Changes made at 2024.03.13_12:45PM

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!11

---

## 0.5.4 [03-11-2024]

* Changes made at 2024.03.11_15:34PM

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!10

---

## 0.5.3 [02-28-2024]

* Changes made at 2024.02.28_11:48AM

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!9

---

## 0.5.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!8

---

## 0.5.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!6

---

## 0.5.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!5

---

## 0.4.0 [09-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!4

---

## 0.3.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!3

---

## 0.2.0 [03-30-2021]

- Added new calls in a swagger that was provided

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!2

---

## 0.1.2 [03-18-2021]

- Change the /org entity paths to /orgs as that seems to be the issues with calls not working.
the swagger is not correct as it has org in all of its paths.
changed 9 paths (19 action/entitypaths)

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!1

---

## 0.1.1 [03-08-2021]

- Initial Commit

See commit ef020b5

---
