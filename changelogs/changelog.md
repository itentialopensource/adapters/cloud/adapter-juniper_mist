
## 0.3.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!3

---

## 0.2.0 [03-30-2021]

- Added new calls in a swagger that was provided

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!2

---

## 0.1.2 [03-18-2021]

- Change the /org entity paths to /orgs as that seems to be the issues with calls not working.
the swagger is not correct as it has org in all of its paths.
changed 9 paths (19 action/entitypaths)

See merge request itentialopensource/adapters/cloud/adapter-juniper_mist!1

---

## 0.1.1 [03-08-2021]

- Initial Commit

See commit ef020b5

---
