# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Juniper_mist System. The API that was used to build the adapter for Juniper_mist is usually available in the report directory of this adapter. The adapter utilizes the Juniper_mist API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Juniper Mist adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Mist to performs instantiation, management, and scalability of a cloud-based wired and wireless infrastructure.

With this adapter you have the ability to perform operations with Juniper Mist such as:

- Organizations
- Networks
- Sites
- Tunnels
- Devices
- Rules

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
