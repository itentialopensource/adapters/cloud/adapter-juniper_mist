## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Juniper Mist. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Juniper Mist.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Juniper Mist. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listsalladdedclouds(callback)</td>
    <td style="padding:15px">Lists all added clouds</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addsanewcloudandreturnsthecloudSid(body, callback)</td>
    <td style="padding:15px">Adds a new cloud and returns the cloud's id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesacloudwithgivencloudId(cloud, callback)</td>
    <td style="padding:15px">Deletes a cloud with given cloud_id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatescloudwithgivencloudId(cloudId, cloud, callback)</td>
    <td style="padding:15px">Updates cloud with given cloud_id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">togglescloudwithgivencloudid(cloudId, cloud, body, callback)</td>
    <td style="padding:15px">Toggles cloud with given cloud id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renamescloudwithgivencloudId(cloud, body, callback)</td>
    <td style="padding:15px">Renames cloud with given cloud_id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listlocationsfromeachcloud(cloud, callback)</td>
    <td style="padding:15px">List locations from each cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listresourcegroups(cloud, callback)</td>
    <td style="padding:15px">List resource groups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/resource-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsizesAkaflavorsFromeachcloud(cloud, callback)</td>
    <td style="padding:15px">List sizes (aka flavors) from each cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/sizes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">liststorageaccounts(cloud, callback)</td>
    <td style="padding:15px">List storage accounts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/storage-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">liststoragepools(cloud, callback)</td>
    <td style="padding:15px">List storage pools</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/storage-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsdatastoresoncloud(cloud, callback)</td>
    <td style="padding:15px">Lists datastores on cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/datastores?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsallDNSzonesbasedonthegivencloudid(cloud, callback)</td>
    <td style="padding:15px">Lists all DNS zones based on the given cloud id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/dns/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsanewDNSzoneunderthegivencloud(cloud, callback)</td>
    <td style="padding:15px">Creates a new DNS zone under the given cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/dns/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesaspecificDNSzoneunderacloud(cloud, zone, callback)</td>
    <td style="padding:15px">Deletes a specific DNS zone under a cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/dns/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsallDNSrecordsforaparticularzone(cloud, zone, callback)</td>
    <td style="padding:15px">Lists all DNS records for a particular zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/dns/zones/{pathv2}/records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsanewrecordunderaspecificzone(cloud, zone, callback)</td>
    <td style="padding:15px">Creates a new record under a specific zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/dns/zones/{pathv2}/records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesaspecificDNSrecordunderazone(record, cloud, zone, callback)</td>
    <td style="padding:15px">Deletes a specific DNS record under a zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/dns/zones/{pathv2}/records/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsallDNSzonesbasedonthegivencloudid1(cloud, callback)</td>
    <td style="padding:15px">Lists all DNS zones based on the given cloud id1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsanewDNSzoneunderthegivencloud1(cloud, callback)</td>
    <td style="padding:15px">Creates a new DNS zone under the given cloud1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesaspecificDNSzoneunderacloud1(cloud, zone, callback)</td>
    <td style="padding:15px">Deletes a specific DNS zone under a cloud1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsallDNSrecordsforaparticularzone1(cloud, zone, callback)</td>
    <td style="padding:15px">Lists all DNS records for a particular zone1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/zones/{pathv2}/records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsanewrecordunderaspecificzone1(cloud, zone, callback)</td>
    <td style="padding:15px">Creates a new record under a specific zone1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/zones/{pathv2}/records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesaspecificDNSrecordunderazone1(record, cloud, zone, callback)</td>
    <td style="padding:15px">Deletes a specific DNS record under a zone1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/zones/{pathv2}/records/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsallthefoldersthatcontainVMs(cloud, callback)</td>
    <td style="padding:15px">Lists all the folders that contain VMs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listimagesofspecifiedcloud(cloud, callback)</td>
    <td style="padding:15px">List images of specified cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchimagesfromcloud(cloud, body, callback)</td>
    <td style="padding:15px">Search images from cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">starUnstaranimage(image, cloud, callback)</td>
    <td style="padding:15px">Star/unstar an image</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/images/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsmachinesoncloudalongwiththeirmetadata(cloud, callback)</td>
    <td style="padding:15px">Lists machines on cloud along with their metadata</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsoneormoremachinesonthespecifiedcloud(cloud, body, callback)</td>
    <td style="padding:15px">Creates one or more machines on the specified cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addamachinetoanOtherServerLibvirtCloud(cloud, body, callback)</td>
    <td style="padding:15px">Add a machine to an OtherServer/Libvirt Cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">callsamachineactiononcloudthatsupportsit(machineUuid, cloud, machine, body, callback)</td>
    <td style="padding:15px">Calls a machine action on cloud that supports it</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editsamachine(cloud, machine, body, callback)</td>
    <td style="padding:15px">Edits a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openVNCconsole(rdpPort, machine, host, cloud, callback)</td>
    <td style="padding:15px">Open VNC console</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/console?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">probesamachine(machine, cloud, body, callback)</td>
    <td style="padding:15px">Probes a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/probe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rdpfileforwindowsmachines(rdpPort, machine, host, cloud, callback)</td>
    <td style="padding:15px">Rdp file for windows machines</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/rdp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">callsamachineactiononcloudthatsupportsit1(machineUuid, body, callback)</td>
    <td style="padding:15px">Calls a machine action on cloud that supports it1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editsamachine1(machineUuid, body, callback)</td>
    <td style="padding:15px">Edits a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openVNCconsole1(rdpPort, machine, host, cloud, machineUuid, callback)</td>
    <td style="padding:15px">Open VNC console1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/console?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openVNCconsole2(rdpPort, machine, host, cloud, machineUuid, callback)</td>
    <td style="padding:15px">Open VNC console2</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/console?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">probesamachine1(machine, cloud, machineUuid, body, callback)</td>
    <td style="padding:15px">Probes a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/probe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rdpfileforwindowsmachines1(rdpPort, machine, host, cloud, machineUuid, callback)</td>
    <td style="padding:15px">Rdp file for windows machines1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/rdp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openSSHconsole(machine, cloud, machineUuid, callback)</td>
    <td style="padding:15px">Open SSH console</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiV1MachinesSshByMachineUuidGET(machineUuid, callback)</td>
    <td style="padding:15px">ApiV1MachinesSshByMachineUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociatesakeyfromamachine(key, machine, cloud, callback)</td>
    <td style="padding:15px">Disassociates a key from a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associatesakeywithamachine(key, machine, cloud, body, callback)</td>
    <td style="padding:15px">Associates a key with a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesmultiplekeys(body, callback)</td>
    <td style="padding:15px">Deletes multiple keys</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsalladdedkeys(callback)</td>
    <td style="padding:15px">Lists all added keys</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateskeypair(callback)</td>
    <td style="padding:15px">Generates key pair</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addskey(body, callback)</td>
    <td style="padding:15px">Adds key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesakey(key, callback)</td>
    <td style="padding:15px">Deletes a key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setsanewdefaultkey(key, callback)</td>
    <td style="padding:15px">Sets a new default key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editsagivenkeySnametonewName(keyId, key, body, callback)</td>
    <td style="padding:15px">Edits a given key's name to new_name</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getsprivatekeyfromkeyname(key, callback)</td>
    <td style="padding:15px">Gets private key from key name</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys/{pathv1}/private?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getspublickeyfromkeyname(key, callback)</td>
    <td style="padding:15px">Gets public key from key name</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/keys/{pathv1}/public?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociatesakeyfromamachine1(key, machine, machineUuid, callback)</td>
    <td style="padding:15px">Disassociates a key from a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associatesakeywithamachine1(key, machine, machineUuid, body, callback)</td>
    <td style="padding:15px">Associates a key with a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateametricfromamachine(machine, cloud, body, callback)</td>
    <td style="padding:15px">Disassociate a metric from a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getallmetricsassociatedwithspecificmachine(id, resourceType, cloud, machine, callback)</td>
    <td style="padding:15px">Get all metrics associated with specific machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateanewmetrictoamachine(machine, cloud, body, callback)</td>
    <td style="padding:15px">Associate a new metric to a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableordisablemonitoringforamachine(machine, cloud, body, callback)</td>
    <td style="padding:15px">Enable or disable monitoring for a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undeployacustompluginScriptfromamachineREADpermissionrequiredoncloudEDITCUSTOMMETRICSpermissionrequiredonmachine(machine, plugin, pluginType, cloud, callback)</td>
    <td style="padding:15px">Undeploy a custom plugin/script from a machine READ permission required on cloud EDIT_CUSTOM_METRIC</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/plugins/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployacustompluginonamachineAddsascriptsWhichisthendeployedonthespecifiedmachinetocollectcustommetrics(unit, valueType, name, pluginType, plugin, machine, readFunction, cloud, callback)</td>
    <td style="padding:15px">Deploy a custom plugin on a machine Adds a scripts, which is then deployed on the specified machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/plugins/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestmonitoringdataforamachine(requestId, step, stop, start, machine, metrics, cloud, callback)</td>
    <td style="padding:15px">Request monitoring data for a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestloaddataforallmonitoredmachines(requestId, start, stop, step, callback)</td>
    <td style="padding:15px">Request load data for all monitored machines</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/stats/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateametricfromamachine1(machine, machineUuid, body, callback)</td>
    <td style="padding:15px">Disassociate a metric from a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateanewmetrictoamachine1(machine, machineUuid, body, callback)</td>
    <td style="padding:15px">Associate a new metric to a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableordisablemonitoringforamachine1(machine, machineUuid, body, callback)</td>
    <td style="padding:15px">Enable or disable monitoring for a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undeployacustompluginScriptfromamachineREADpermissionrequiredoncloudEDITCUSTOMMETRICSpermissionrequiredonmachine1(machine, plugin, pluginType, machineUuid, callback)</td>
    <td style="padding:15px">Undeploy a custom plugin/script from a machine READ permission required on cloud EDIT_CUSTOM_METRIC</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/plugins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployacustompluginonamachineAddsascriptsWhichisthendeployedonthespecifiedmachinetocollectcustommetrics1(unit, valueType, name, pluginType, plugin, machine, readFunction, machineUuid, callback)</td>
    <td style="padding:15px">Deploy a custom plugin on a machine Adds a scripts, which is then deployed on the specified machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/plugins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestmonitoringdataforamachine1(requestId, step, stop, start, machine, metrics, machineUuid, callback)</td>
    <td style="padding:15px">Request monitoring data for a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/machines/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getallmetricsassociatedwithspecificmachine1(id, resourceType, callback)</td>
    <td style="padding:15px">Get all metrics associated with specific machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateametricconfigurationREADpermissionrequiredoncloudEDITCUSTOMMETRICSpermissionrequiredonmachine(metric, unit, cloudId, name, machineId, callback)</td>
    <td style="padding:15px">Update a metric configuration READ permission required on cloud EDIT_CUSTOM_METRICS permission requ</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setglobalemailalertsRecipients(body, callback)</td>
    <td style="padding:15px">Set global email alerts' recipients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">liststagsofamachine(machine, cloudId, cloud, callback)</td>
    <td style="padding:15px">Lists tags of a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setstagsforamachineGiventhecloudandmachineid(machineId, cloudId, cloud, machine, callback)</td>
    <td style="padding:15px">Sets tags for a machine, given the cloud and machine id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletestaginthedbforspecifiedresourceType(machineId, cloudId, cloud, machine, tag, body, callback)</td>
    <td style="padding:15px">Deletes tag in the db for specified resource_type</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/machines/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchoperationforaddingRemovingtagsfromalistofresources(body, callback)</td>
    <td style="padding:15px">Batch operation for adding/removing tags from a list of resources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthenetworksofacloudREADpermissionrequiredoncloudNetworksAndsubnets(cloud, callback)</td>
    <td style="padding:15px">List the networks of a cloud READ permission required on cloud, networks, and subnets</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewnetworkIfsubnetparametersarespecifiedTheywillbeusedtocreateanewsubnetinthenewlycreatednetwork(cloudId, cloud, body, callback)</td>
    <td style="padding:15px">Create a new network If subnet parameters are specified, they will be used to create a new subnet i</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteanetworkandallcorrespondingsubnetsREADpermissionrequiredoncloudREADpermissionrequiredonnetworkREMOVEpermissionrequiredonnetwork(cloudId, networkId, cloud, network, callback)</td>
    <td style="padding:15px">Delete a network and all corresponding subnets READ permission required on cloud READ permission re</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associatesipwiththespecificnetworkandmachine(cloud, network, body, callback)</td>
    <td style="padding:15px">Associates ip with the specific network and machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthesubnetsofanetworkREADpermissionrequiredoncloudREADpermissionrequiredonnetworkREADpermissionrequiredonsubnets(networkId, cloud, network, callback)</td>
    <td style="padding:15px">List the subnets of a network READ permission required on cloud READ permission required on network</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks/{pathv2}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsubnetinagivennetworkonacloudADDpermissionrequiredonsubnetREADpermissionrequiredoncloudREADpermissionrequiredonnetworkCREATESUBNETSpermissionrequiredonnetworkCREATERESOURCESpermissionrequiredoncloud(networkId, cloudId, cloud, network, body, callback)</td>
    <td style="padding:15px">Create subnet in a given network on a cloud ADD permission required on subnet READ permission requi</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks/{pathv2}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteasubnetREADpermissionrequiredoncloudREADpermissionrequiredonnetworkREADpermissionrequiredonsubnetREMOVEpermissionrequiredonsubnet(networkId, cloudId, subnetId, cloud, network, subnet, callback)</td>
    <td style="padding:15px">Delete a subnet READ permission required on cloud READ permission required on network READ permissi</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/networks/{pathv2}/subnets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthevirtualnetworkfunctionsofacloudKVMonlyREADpermissionrequiredoncloud(cloud, callback)</td>
    <td style="padding:15px">List the virtual network functions of a cloud (KVM only) READ permission required on cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/vnfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthenetworksofacloudREADpermissionrequiredoncloudNetworksAndsubnets1(cloud, callback)</td>
    <td style="padding:15px">List the networks of a cloud READ permission required on cloud, networks, and subnets1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletetheportforwardofaGigG8networkREADpermissionrequiredonnetworkEDITpermissionrequiredonnetwork(network, body, callback)</td>
    <td style="padding:15px">Delete the portforward of a GigG8 network READ permission required on network EDIT permission requi</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/networks/{pathv1}/portforwards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheportforwardsofaGigG8networkREADpermissionrequiredonnetwork(network, callback)</td>
    <td style="padding:15px">List the portforwards of a GigG8 network READ permission required on network</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/networks/{pathv1}/portforwards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createaportforwardinaGigG8networkREADpermissionrequiredonnetworkEDITpermissionrequiredonnetwork(network, body, callback)</td>
    <td style="padding:15px">Create a portforward in a GigG8 network READ permission required on network EDIT permission require</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/networks/{pathv1}/portforwards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsprojectsoncloud(cloud, callback)</td>
    <td style="padding:15px">Lists projects on cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listssecuritygroupsoncloud(cloud, callback)</td>
    <td style="padding:15px">Lists security groups on cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthevolumesofacloud(cloud, callback)</td>
    <td style="padding:15px">List the volumes of a cloud</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/storage-classes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthevolumesofacloud1(cloud, callback)</td>
    <td style="padding:15px">List the volumes of a cloud1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewvolume(cloud, body, callback)</td>
    <td style="padding:15px">Create a new volume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteavolume(cloud, volume, callback)</td>
    <td style="padding:15px">Delete a volume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/volumes/*volume_ext?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachordetachavolumetoFromamachine(volume, machine, device, cloud, callback)</td>
    <td style="padding:15px">Attach or detach a volume to/from a machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/clouds/{pathv1}/volumes/*volume_ext?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthevolumesofacloud2(cloud, callback)</td>
    <td style="padding:15px">List the volumes of a cloud2</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteavolume1(cloud, volume, callback)</td>
    <td style="padding:15px">Delete a volume1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/volumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachordetachavolumetoFromamachine1(volume, machine, device, cloud, callback)</td>
    <td style="padding:15px">Attach or detach a volume to/from a machine1</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/volumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">endsarunningjob(jobId, callback)</td>
    <td style="padding:15px">Ends a running job</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesastory(jobId, callback)</td>
    <td style="padding:15px">Fetches a story</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getsthelatestlogs(callback)</td>
    <td style="padding:15px">Gets the latest logs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">closesanopenstory(storyId, callback)</td>
    <td style="padding:15px">Closes an open story</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/stories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnotificationoverridesforuserOrgpolicy(callback)</td>
    <td style="padding:15px">Get notification overrides for user, org policy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/notification-overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanotificationoverridewiththespecifiedproperties(notification, callback)</td>
    <td style="padding:15px">Add a notification override with the specified properties</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/notification-overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteanotificationoverride(overrideId, callback)</td>
    <td style="padding:15px">Delete a notification override</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/notification-overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dismissaninAppnotification(notification, notificationId, callback)</td>
    <td style="padding:15px">Dismiss an in-app notification</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/notifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsorganization(body, callback)</td>
    <td style="padding:15px">Creates organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showorganization(orgId, callback)</td>
    <td style="padding:15px">Show organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editanorganizationentryinthedbMeansrename(orgId, body, callback)</td>
    <td style="padding:15px">Edit an organization entry in the db Means rename</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesmultipleteams(orgId, body, callback)</td>
    <td style="padding:15px">Deletes multiple teams</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">liststeamsofanorg(orgId, callback)</td>
    <td style="padding:15px">Lists teams of an org</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsnewteam(orgId, body, callback)</td>
    <td style="padding:15px">Creates new team</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesateamentryinthedb(orgId, teamId, body, callback)</td>
    <td style="padding:15px">Deletes a team entry in the db</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showteam(orgId, teamId, callback)</td>
    <td style="padding:15px">Show team</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renamesateamentryinthedb(orgId, teamId, body, callback)</td>
    <td style="padding:15px">Renames a team entry in the db</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inviteamembertoteam(orgId, teamId, body, callback)</td>
    <td style="padding:15px">Invite a member to team</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteateamSmemberentryfromthedb(orgId, teamId, userId, body, callback)</td>
    <td style="padding:15px">Delete a team's member entry from the db</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/members/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listteamSpolicy(orgId, teamId, callback)</td>
    <td style="padding:15px">List team's policy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editteampolicyoperatorAkadefaultrule(orgId, teamId, body, callback)</td>
    <td style="padding:15px">Edit team policy operator (aka default rule)</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editteampolicyoperatorAkadefaultruleSpecificapipointforthemomentoffirstiteration(orgId, teamId, body, callback)</td>
    <td style="padding:15px">Edit team policy operator (aka default rule) Specific api point for the moment of first iteration</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addruletopolicyruleslistOnlyavailabletoorganizationowners(orgId, teamId, body, callback)</td>
    <td style="padding:15px">Add rule to policy rules list Only available to organization owners</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteruleItmeanspopitfromthelistandsaveteam(orgId, teamId, indexId, body, callback)</td>
    <td style="padding:15px">Delete rule It means pop it from the list and save team</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchruleLikeeditrulewiththefollowingdifferences1NotallfieldsneedtobespecifiedOnlytheonesbeingmodified2Extrafieldposissupportedtomovetheruleupordownthelist(orgId, teamId, indexId, body, callback)</td>
    <td style="padding:15px">Patch rule Like edit rule with the following differences: 1)not all fields need to be specified, on</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertruletopolicyruleslistbyindexidOnlyavailabletoorganizationowners(orgId, teamId, indexId, body, callback)</td>
    <td style="padding:15px">Insert rule to policy rules list by index id Only available to organization owners</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editReplaceoneruleofpolicyruleslistItmeansinsertitatthepolicyruleslistbyindexidonlyridorrtag(orgId, teamId, indexId, body, callback)</td>
    <td style="padding:15px">Edit/replace one rule of policy rules list It means insert it at the policy rules list by index id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/teams/{pathv2}/policy/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listssupportedproviders(callback)</td>
    <td style="padding:15px">Lists supported providers</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processatriggersentbythealertservice(body, callback)</td>
    <td style="padding:15px">Process a trigger sent by the alert service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/rule-triggered?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanewruleREADpermissionrequiredonCloudEDITRULESpermissionrequiredonMachine(selectors, actions, queries, frequency, window, triggerAfter, callback)</td>
    <td style="padding:15px">Add a new rule READ permission required on Cloud EDIT_RULES permission required on Machine</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletearulegivenitsUUID(rule, callback)</td>
    <td style="padding:15px">Delete a rule given its UUID</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renamearule(rule, title, callback)</td>
    <td style="padding:15px">Rename a rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatearulegivenitsUUIDTheexpectedrequestbodyisthesameasfortheAddRuleEndpoint(ruleId, rule, callback)</td>
    <td style="padding:15px">Update a rule given its UUID The expected request body is the same as for the `add_rule` endpoint</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableordisablearulePermitsOwnerstotemporarilydisableorreEnablearuleSevaluation(rule, action, callback)</td>
    <td style="padding:15px">Enable or disable a rule Permits Owners to temporarily disable or re-enable a rule's evaluation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsuserschedulesentriesOrderbyId(callback)</td>
    <td style="padding:15px">Lists user schedules entries, order by _id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addsanentrytouserschedules(body, callback)</td>
    <td style="padding:15px">Adds an entry to user schedules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesascheduleentryofauser(scheduleId, body, callback)</td>
    <td style="padding:15px">Deletes a schedule entry of a user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showdetailsofschedule(scheduleId, callback)</td>
    <td style="padding:15px">Show details of schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editascheduleentry(scheduleId, body, callback)</td>
    <td style="padding:15px">Edit a schedule entry</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesmultiplescripts(body, callback)</td>
    <td style="padding:15px">Deletes multiple scripts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsuserscripts(callback)</td>
    <td style="padding:15px">Lists user scripts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addscripttouserscripts(body, callback)</td>
    <td style="padding:15px">Add script to user scripts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesscript(scriptId, callback)</td>
    <td style="padding:15px">Deletes script</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showscriptdetailsandjobhistory(scriptId, callback)</td>
    <td style="padding:15px">Show script details and job history</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startascriptjobtorunthescript(scriptId, body, callback)</td>
    <td style="padding:15px">Start a script job to run the script</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editscriptRenameonlyasfornow(scriptId, body, callback)</td>
    <td style="padding:15px">Edit script (rename only as for now)</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadscriptfileorarchive(scriptId, callback)</td>
    <td style="padding:15px">Download script file or archive</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts/{pathv1}/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnstoamistauthenticateduserAselfAuthSignedurlforfetchingascriptSfile(scriptId, callback)</td>
    <td style="padding:15px">Returns to a mist authenticated user, a self-auth/signed url for fetching a script's file</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/scripts/{pathv1}/url?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeanactivesession(body, callback)</td>
    <td style="padding:15px">Revoke an active session</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsactivesessions(callback)</td>
    <td style="padding:15px">Lists active sessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addtemplatetouserOrgtemplates(body, callback)</td>
    <td style="padding:15px">Add template to user/org templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletetemplategivenREMOVEpermissionhasbeengrantedUsersmaydeleteatemplate(templateId, body, callback)</td>
    <td style="padding:15px">Delete template given REMOVE permission has been granted Users may delete a template</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edittemplategivenEDITpermissionhasbeengrantedUsersmayeditatemplateSnameanddescription(templateId, body, callback)</td>
    <td style="padding:15px">Edit template given EDIT permission has been granted Users may edit a template's name and descripti</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teardownanexistingVPNTunnel(tunnelId, callback)</td>
    <td style="padding:15px">Tear down an existing VPN Tunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/tunnels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ashellcommandtoeasilyretrieveandexecutetheVPNclientSconfigurationscriptTheCurlCommandisalsosignedwithanHMAC(tunnelId, callback)</td>
    <td style="padding:15px">A shell command to easily retrieve and execute the VPN client's configuration script The `curl` com</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/tunnels/{pathv1}/command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vPNclientconfigurationscriptRequestaVPNclientSconfigurationscriptonthefly(tunnelId, callback)</td>
    <td style="padding:15px">VPN client configuration script Request a VPN client's configuration script on the fly</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/tunnels/{pathv1}/script?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serveblogindexoverapi(callback)</td>
    <td style="padding:15px">Serve blog index over api</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/blog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerAlarmTemplates(orgId, callback)</td>
    <td style="padding:15px">getInstallerAlarmTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/alarmtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerDeviceProfilesWIP(orgId, callback)</td>
    <td style="padding:15px">getInstallerDeviceProfiles_WIP_</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/deviceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerListOfRenctlyClaimedDevices(orgId, callback)</td>
    <td style="padding:15px">getInstallerListOfRenctlyClaimedDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimInstallerDevices(orgId, body, callback)</td>
    <td style="padding:15px">claimInstallerDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignInstallerRecentlyClaimedDevice(orgId, deviceMac, callback)</td>
    <td style="padding:15px">unassignInstallerRecentlyClaimedDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionInstallerDevices(orgId, deviceMac, body, callback)</td>
    <td style="padding:15px">provisionInstallerDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startInstallerLocateDevice(orgId, deviceMac, callback)</td>
    <td style="padding:15px">startInstallerLocateDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices/{pathv2}/locate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopInstallerLocateDevice(orgId, deviceMac, callback)</td>
    <td style="padding:15px">stopInstallerLocateDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices/{pathv2}/unlocate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInstallerDeviceImage(orgId, imageName, deviceMac, callback)</td>
    <td style="padding:15px">deleteInstallerDeviceImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installerDeviceImage(orgId, imageName, deviceMac, bodyFormData, callback)</td>
    <td style="padding:15px">InstallerDeviceImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/devices/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerRfTemplatesNames(orgId, callback)</td>
    <td style="padding:15px">getInstallerRfTemplatesNames</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/rftemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerSecPolicies(orgId, callback)</td>
    <td style="padding:15px">getInstallerSecPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/secpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerSiteGroups(orgId, callback)</td>
    <td style="padding:15px">getInstallerSiteGroups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sitegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerSites(orgId, callback)</td>
    <td style="padding:15px">getInstallerSites</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateInstallerSites(orgId, siteName, body, callback)</td>
    <td style="padding:15px">createOrUpdateInstallerSites</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallerMaps(orgId, siteName, callback)</td>
    <td style="padding:15px">getInstallerMaps</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites/{pathv2}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importInstallerMap(orgId, siteName, csv, file, json, callback)</td>
    <td style="padding:15px">importInstallerMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites/{pathv2}/maps/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInstallerMap(orgId, siteName, mapId, callback)</td>
    <td style="padding:15px">deleteInstallerMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites/{pathv2}/maps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInstallerMap(orgId, siteName, mapId, body, callback)</td>
    <td style="padding:15px">createInstallerMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites/{pathv2}/maps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInstallerMap(orgId, siteName, mapId, body, callback)</td>
    <td style="padding:15px">updateInstallerMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/orgs/{pathv1}/sites/{pathv2}/maps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">optimizeInstallerRrm(siteName, callback)</td>
    <td style="padding:15px">optimizeInstallerRrm</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/installer/sites/{pathv1}/optimize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyAdminInvite(token, callback)</td>
    <td style="padding:15px">verifyAdminInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/invite/verify/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateSdkInvite(secret, body, callback)</td>
    <td style="padding:15px">activateSdkInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/mobile/verify/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMsp(body, callback)</td>
    <td style="padding:15px">createMsp</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMsp(mspId, callback)</td>
    <td style="padding:15px">deleteMsp</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMsp(mspId, body, callback)</td>
    <td style="padding:15px">updateMsp</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspAdmins(mspId, callback)</td>
    <td style="padding:15px">getMspAdmins</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeMspAdmin(mspId, adminId, callback)</td>
    <td style="padding:15px">revokeMspAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspAdmin(mspId, adminId, callback)</td>
    <td style="padding:15px">getMspAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMspAdmin(mspId, adminId, body, callback)</td>
    <td style="padding:15px">updateMspAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspInventoryByMac(mspId, mac, callback)</td>
    <td style="padding:15px">getMspInventoryByMac</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/inventory/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inviteMspAdmin(mspId, body, callback)</td>
    <td style="padding:15px">inviteMspAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/invites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uninviteMspAdmin(mspId, inviteId, callback)</td>
    <td style="padding:15px">uninviteMspAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/invites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMspAdminInvite(mspId, inviteId, body, callback)</td>
    <td style="padding:15px">updateMspAdminInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/invites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspOrgGroups(mspId, callback)</td>
    <td style="padding:15px">getMspOrgGroups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orggroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMspOrgGroup(mspId, body, callback)</td>
    <td style="padding:15px">createMspOrgGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orggroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMspOrgGroup(mspId, orggroupId, callback)</td>
    <td style="padding:15px">deleteMspOrgGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orggroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspOrgGroup(mspId, orggroupId, callback)</td>
    <td style="padding:15px">getMspOrgGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orggroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMspOrgGroup(mspId, orggroupId, body, callback)</td>
    <td style="padding:15px">updateMspOrgGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orggroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspOrgs(mspId, callback)</td>
    <td style="padding:15px">getMspOrgs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMspOrg(mspId, body, callback)</td>
    <td style="padding:15px">createMspOrg</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageMspOrgs(mspId, body, callback)</td>
    <td style="padding:15px">manageMspOrgs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspOrg(mspId, orgId, callback)</td>
    <td style="padding:15px">getMspOrg</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/orgs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchMspOrgGroup(mspId, type = 'orgs', q, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchMspOrgGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspSsoRoles(mspId, callback)</td>
    <td style="padding:15px">getMspSsoRoles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssoroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMspSsoRole(mspId, body, callback)</td>
    <td style="padding:15px">createMspSsoRole</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssoroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMspSsoRoles(mspId, ssoroleId, callback)</td>
    <td style="padding:15px">deleteMspSsoRoles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssoroles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMspSsoRole(mspId, ssoroleId, body, callback)</td>
    <td style="padding:15px">updateMspSsoRole</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssoroles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspSso(mspId, callback)</td>
    <td style="padding:15px">getMspSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMspSso(mspId, body, callback)</td>
    <td style="padding:15px">createMspSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMspSso(mspId, ssoId, callback)</td>
    <td style="padding:15px">deleteMspSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMspSso(mspId, ssoId, body, callback)</td>
    <td style="padding:15px">updateMspSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspSsoLatestFailures(mspId, ssoId, callback)</td>
    <td style="padding:15px">getMspSsoLatestFailures</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos/{pathv2}/failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspSsoSamlMetadata(mspId, ssoId, callback)</td>
    <td style="padding:15px">getMspSsoSamlMetadata</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadMspSsoSamlMetadata(mspId, ssoId, callback)</td>
    <td style="padding:15px">downloadMspSsoSamlMetadata</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/ssos/{pathv2}/metadata.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspOrgLicenses(mspId, callback)</td>
    <td style="padding:15px">createMsp</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/stats/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMspOrgStats(mspId, page, limit, callback)</td>
    <td style="padding:15px">getMspOrgStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/msps/{pathv1}/stats/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrg(orgId, callback)</td>
    <td style="padding:15px">deleteOrg</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgAdmins(orgId, callback)</td>
    <td style="padding:15px">getOrgAdmins</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeOrgAdmin(orgId, adminId, callback)</td>
    <td style="padding:15px">revokeOrgAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgAdmin(orgId, adminId, body, callback)</td>
    <td style="padding:15px">updateOrgAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgAlarms(orgId, distinct, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarms/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchOrgAlarms(orgId, type, start, end, duration = '1d', limit, page, callback)</td>
    <td style="padding:15px">searchOrgAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarms/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgAlarmTemplates(orgId, callback)</td>
    <td style="padding:15px">getOrgAlarmTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarmtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgAlarmTemplate(orgId, body, callback)</td>
    <td style="padding:15px">createOrgAlarmTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarmtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suppressOrgAlarmTemplate(orgId, body, callback)</td>
    <td style="padding:15px">suppressOrgAlarmTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarmtemplates/suppress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgAlarmTemplate(orgId, alarmtemplateId, callback)</td>
    <td style="padding:15px">deleteOrgAlarmTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarmtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgAlarmTemplate(orgId, alarmtemplateId, callback)</td>
    <td style="padding:15px">getOrgAlarmTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarmtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgAlarmTemplate(orgId, alarmtemplateId, body, callback)</td>
    <td style="padding:15px">updateOrgAlarmTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/alarmtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgApiTokens(orgId, callback)</td>
    <td style="padding:15px">getOrgApiTokens</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgApiToken(orgId, callback)</td>
    <td style="padding:15px">createOrgApiToken</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgApiToken(orgId, apitokenId, callback)</td>
    <td style="padding:15px">deleteOrgApiToken</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/apitokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgApiToken(orgId, apitokenId, callback)</td>
    <td style="padding:15px">getOrgApiToken</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/apitokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgApiToken(orgId, apitokenId, body, callback)</td>
    <td style="padding:15px">updateOrgApiToken</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/apitokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgAssetFilters(orgId, callback)</td>
    <td style="padding:15px">getOrgAssetFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/assetfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgAssetFilters(orgId, body, callback)</td>
    <td style="padding:15px">createOrgAssetFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/assetfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgAssetFilters(orgId, assetfilterId, callback)</td>
    <td style="padding:15px">deleteOrgAssetFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/assetfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgAssetFilter(orgId, assetfilterId, callback)</td>
    <td style="padding:15px">getOrgAssetFilter</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/assetfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgAssetFilters(orgId, assetfilterId, body, callback)</td>
    <td style="padding:15px">updateOrgAssetFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/assetfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgCertificates(orgId, callback)</td>
    <td style="padding:15px">getOrgCertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/cert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearOrgCertificates(orgId, callback)</td>
    <td style="padding:15px">clearOrgCertificates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/cert/regenerate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimOrgLicense(orgId, body, callback)</td>
    <td style="padding:15px">claimOrgLicense</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgWirelessClients(orgId, distinct = 'mac', mac, hostname, device, os, model, ap, vlan, ssid, ip, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgWirelessClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/clients/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchOrgWirelessClients(orgId, mac, hostname, device, os, model, ap, vlan, ssid, ip, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchOrgWirelessClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/clients/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgWirelessClientsSessions(orgId, distinct = 'ssid', ap, band = '24', clientFamily, clientManufacture, clientModel, clientOs, ssid, wlanId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getMxEdgeModels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/clients/sessions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchOrgWirelessClientSessions(orgId, ap, band = '24', clientFamily, clientManufacture, clientModel, clientOs, ssid, wlanId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchOrgWirelessClientSessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/clients/sessions/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneOrg(orgId, body, callback)</td>
    <td style="padding:15px">cloneOrg</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgCrlFile(orgId, callback)</td>
    <td style="padding:15px">getOrgCrlFile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgDeviceProfiles(orgId, callback)</td>
    <td style="padding:15px">getOrgDeviceProfiles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgDeviceProfiles(orgId, body, callback)</td>
    <td style="padding:15px">createOrgDeviceProfiles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgDeviceProfile(orgId, deviceprofileId, callback)</td>
    <td style="padding:15px">deleteOrgDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgDeviceProfile(orgId, deviceprofileId, callback)</td>
    <td style="padding:15px">getOrgDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgDeviceProfile(orgId, deviceprofileId, body, callback)</td>
    <td style="padding:15px">updateOrgDeviceProfiles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignOrgDeviceProfileToDevices(orgId, deviceprofileId, body, callback)</td>
    <td style="padding:15px">assignOrgDeviceProfileToDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles/{pathv2}/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignOrgDeviceProfilesFromDevices(orgId, deviceprofileId, body, callback)</td>
    <td style="padding:15px">unassignOrgDeviceProfilesFromDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/deviceprofiles/{pathv2}/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgDevices(orgId, callback)</td>
    <td style="padding:15px">getOrgDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgDevices(orgId, distinct = 'hostname', hostname, siteId, model, mac, version, ip, mxtunnelStatus = 'up', mxedgeId, lldpSystemName, lldpSystemDesc, lldpPortId, lldpMgmtAddr, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/devices/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgDevicesEvents(distinct = 'org_id', orgId, siteId, ap, apfw, model, text, timestamp, type = 'AP_CONFIG_CHANGED_BY_RRM', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgDevicesEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/devices/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchOrgDevicesEvents(orgId, siteId, ap, apfw, model, text, timestamp, type = 'AP_CONFIG_CHANGED_BY_RRM', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchOrgDevicesEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/devices/events/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchOrgDevices(orgId, hostname, siteId, model, mac, version, ip, mxtunnelStatus = 'up', mxedgeId, lldpSystemName, lldpSystemDesc, lldpPortId, lldpMgmtAddr, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchOrgDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/devices/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgJuniperDevicesCommand(orgId, callback)</td>
    <td style="padding:15px">getOrgJuniperDevicesCommand</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ocdevices/outbound_ssh_cmd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgInventory(orgId, serial, model, type, mac, siteId, vcMac, vc, callback)</td>
    <td style="padding:15px">getOrgInventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOrgInventory(orgId, body, callback)</td>
    <td style="padding:15px">addOrgInventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgInventoryAssignment(orgId, body, callback)</td>
    <td style="padding:15px">updateOrgInventoryAssignment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceOrgDevices(orgId, body, callback)</td>
    <td style="padding:15px">replaceOrgDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/inventory/replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inviteOrgAdmin(orgId, body, callback)</td>
    <td style="padding:15px">inviteOrgAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/invites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uninviteOrgAdmin(orgId, inviteId, callback)</td>
    <td style="padding:15px">uninviteOrgAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/invites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgAdminInvite(orgId, inviteId, body, callback)</td>
    <td style="padding:15px">updateOrgAdminInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/invites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgLicencesSummary(orgId, callback)</td>
    <td style="padding:15px">getOrgLicencesSummary</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveOrDeleteOrgLicenseToAnotherOrg(orgId, body, callback)</td>
    <td style="padding:15px">moveOrDeleteOrgLicenseToAnotherOrg</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgLicencesBySite(orgId, callback)</td>
    <td style="padding:15px">getOrgLicencesBySite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/licenses/usages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgLogs(orgId, siteId, adminName, message, start, end, limit, page, duration = '1d', callback)</td>
    <td style="padding:15px">getOrgLogs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgLogsByDistinctAttributes(orgId, distinct = 'admin_id', adminId, adminName, siteId, message, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgLogsByDistinctAttributes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/logs/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdgeClusters(orgId, callback)</td>
    <td style="padding:15px">getOrgMxEdgeClusters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxclusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgMxEdgeCluster(orgId, body, callback)</td>
    <td style="padding:15px">createOrgMxEdgeCluster</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxclusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgMxEdgeCluster(orgId, mxclusterId, callback)</td>
    <td style="padding:15px">deleteOrgMxEdgeCluster</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxclusters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdgeCluster(orgId, mxclusterId, callback)</td>
    <td style="padding:15px">getOrgMxEdgeCluster</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxclusters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgMxEdgeCluster(orgId, mxclusterId, body, callback)</td>
    <td style="padding:15px">updateOrgMxEdgeCluster</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxclusters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdges(orgId, callback)</td>
    <td style="padding:15px">getOrgMxEdges</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgMxEdge(orgId, body, callback)</td>
    <td style="padding:15px">createOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignOrgMxEdgeToSite(orgId, body, callback)</td>
    <td style="padding:15px">assignOrgMxEdgeToSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimOrgMxEdge(orgId, body, callback)</td>
    <td style="padding:15px">claimOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignOrgMxEdgeFromSite(orgId, body, callback)</td>
    <td style="padding:15px">unassignOrgMxEdgeFromSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdgeUpgradeInfo(orgId, channel = 'stable', callback)</td>
    <td style="padding:15px">getOrgMxEdgeUpgradeInfo</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgMxEdge(orgId, mxedgeId, callback)</td>
    <td style="padding:15px">deleteOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdge(orgId, mxedgeId, callback)</td>
    <td style="padding:15px">getOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgMxEdge(orgId, mxedgeId, body, callback)</td>
    <td style="padding:15px">updateOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartOrgMxEdge(orgId, mxedgeId, callback)</td>
    <td style="padding:15px">restartOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unregisterOrgMxEdge(orgId, mxedgeId, callback)</td>
    <td style="padding:15px">unregisterOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}/unregister?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeOrgMxEdge(orgId, mxedgeId, body, callback)</td>
    <td style="padding:15px">upgradeOrgMxEdge</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">controlOrgMxEdgeServices(orgId, mxedgeId, name = 'tunterm', action = 'restart', callback)</td>
    <td style="padding:15px">controlOrgMxEdgeServices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/{pathv2}/services/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeOrgMxEdges(orgId, body, callback)</td>
    <td style="padding:15px">upgradeOrgMxEdges</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxedges/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxTunnels(orgId, callback)</td>
    <td style="padding:15px">getOrgMxTunnels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxtunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgMxTunnel(orgId, body, callback)</td>
    <td style="padding:15px">createOrgMxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxtunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgMxTunnel(orgId, mxtunnelId, callback)</td>
    <td style="padding:15px">deleteOrgMxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxTunnel(orgId, mxtunnelId, callback)</td>
    <td style="padding:15px">getOrgMxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgMxTunnel(orgId, mxtunnelId, body, callback)</td>
    <td style="padding:15px">updateOrgMxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/mxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgNetworkTemplates(orgId, callback)</td>
    <td style="padding:15px">getOrgNetworkTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networktemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgNetworkTemplate(orgId, body, callback)</td>
    <td style="padding:15px">createOrgNetworkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networktemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgNetworkTemplate(orgId, networktemplateId, callback)</td>
    <td style="padding:15px">getOrgNetworkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networktemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgNetworkTemplates(orgId, networktemplateId, body, callback)</td>
    <td style="padding:15px">updateOrgNetworkTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networktemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgNetworkTemplate(orgId, networktemplateId, callback)</td>
    <td style="padding:15px">deleteOrgNetworkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networktemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgPsks(orgId, name, callback)</td>
    <td style="padding:15px">getOrgPsks</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/psks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgPsk(orgId, body, callback)</td>
    <td style="padding:15px">createOrgPsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/psks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importOrgPskJson(orgId, file, callback)</td>
    <td style="padding:15px">importOrgPskJson</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/psks/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgPsk(orgId, pskId, callback)</td>
    <td style="padding:15px">deleteOrgPsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/psks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgPsk(orgId, pskId, callback)</td>
    <td style="padding:15px">getOrgPsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/psks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgPsk(orgId, pskId, body, callback)</td>
    <td style="padding:15px">updateOrgPsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/psks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importSitePskJson(siteId, file, callback)</td>
    <td style="padding:15px">importSitePskJson</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/psks/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitePsks(siteId, name, page, callback)</td>
    <td style="padding:15px">getSitePsks</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/psks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSitePsk(siteId, body, callback)</td>
    <td style="padding:15px">createSitePsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/psks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitePsk(siteId, pskId, callback)</td>
    <td style="padding:15px">deleteSitePsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/psks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitePsk(siteId, pskId, callback)</td>
    <td style="padding:15px">getSitePsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/psks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSitePsk(siteId, pskId, body, callback)</td>
    <td style="padding:15px">updateSitePsk</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/psks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgRfTemplates(orgId, callback)</td>
    <td style="padding:15px">getOrgRfTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/rftemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgRfTemplate(orgId, body, callback)</td>
    <td style="padding:15px">createOrgRfTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/rftemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgRfTemplate(orgId, rftemplateId, callback)</td>
    <td style="padding:15px">deleteOrgRfTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/rftemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgRfTemplate(orgId, rftemplateId, callback)</td>
    <td style="padding:15px">getOrgRfTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/rftemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgRfTemplate(orgId, rftemplateId, body, callback)</td>
    <td style="padding:15px">updateOrgRfTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/rftemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSdkClient(orgId, sdkclientId, body, callback)</td>
    <td style="padding:15px">updateSdkClient</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkclients/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSdkInvites(orgId, callback)</td>
    <td style="padding:15px">getSdkInvites</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSdkInvite(orgId, body, callback)</td>
    <td style="padding:15px">createSdkInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeSdkInvite(orgId, sdkinviteId, callback)</td>
    <td style="padding:15px">revokeSdkInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSdkInvite(orgId, sdkinviteId, callback)</td>
    <td style="padding:15px">getSdkInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSdkInvite(orgId, sdkinviteId, body, callback)</td>
    <td style="padding:15px">updateSdkInvite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendSdkInviteEmail(orgId, sdkinviteId, body, callback)</td>
    <td style="padding:15px">sendSdkInviteEmail</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites/{pathv2}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSdkInviteQrCode(orgId, sdkinviteId, callback)</td>
    <td style="padding:15px">getSdkInviteQrCode</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites/{pathv2}/qrcode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendSdkInviteSms(orgId, sdkinviteId, body, callback)</td>
    <td style="padding:15px">sendSdkInviteSms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdkinvites/{pathv2}/sms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSdkTemplates(orgId, callback)</td>
    <td style="padding:15px">getSdkTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdktemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSdkTemplate(orgId, body, callback)</td>
    <td style="padding:15px">createSdkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdktemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSdkTemplate(orgId, sdktemplateId, callback)</td>
    <td style="padding:15px">deleteSdkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdktemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSdkTemplate(orgId, sdktemplateId, callback)</td>
    <td style="padding:15px">getSdkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdktemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSdkTemplate(orgId, sdktemplateId, body, callback)</td>
    <td style="padding:15px">updateSdkTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sdktemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSecPolicies(orgId, callback)</td>
    <td style="padding:15px">getOrgSecPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/secpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgSecPolicies(orgId, body, callback)</td>
    <td style="padding:15px">createOrgSecPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/secpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgSecPolicies(orgId, secpolicyId, callback)</td>
    <td style="padding:15px">deleteOrgSecPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/secpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSecPolicy(orgId, secpolicyId, callback)</td>
    <td style="padding:15px">getOrgSecPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/secpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgSecPolicies(orgId, secpolicyId, body, callback)</td>
    <td style="padding:15px">updateOrgSecPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/secpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSettings(orgId, callback)</td>
    <td style="padding:15px">getOrgSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgSettings(orgId, body, callback)</td>
    <td style="padding:15px">updateOrgSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setOrgCustomBucket(orgId, body, callback)</td>
    <td style="padding:15px">setOrgCustomBucket</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/setting/pcap_bucket/setup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyOrgCustomBucket(orgId, body, callback)</td>
    <td style="padding:15px">verifyOrgCustomBucket</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/setting/pcap_bucket/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSiteGroups(orgId, callback)</td>
    <td style="padding:15px">getOrgSiteGroups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sitegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgSiteGroup(orgId, body, callback)</td>
    <td style="padding:15px">createOrgSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sitegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgSiteGroup(orgId, sitegroupId, callback)</td>
    <td style="padding:15px">deleteOrgSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sitegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgSiteGroup(orgId, sitegroupId, body, callback)</td>
    <td style="padding:15px">updateOrgSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sitegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSiteGroup(orgId, sitegroupId, callback)</td>
    <td style="padding:15px">getOrgSiteGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sitegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSites(orgId, callback)</td>
    <td style="padding:15px">getOrgSites</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgSite(orgId, body, callback)</td>
    <td style="padding:15px">createOrgSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importOrgMapToSite(orgId, siteName, csl, file, callback)</td>
    <td style="padding:15px">importOrgMapToSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sites/{pathv2}/maps/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchOrgSites(orgId, analyticEnabled, appWaking, assetEnabled, autoUpgradeEnabled, autoUpgradeVersion, countryCode, honeypotEnabled, id, locateUnconnected, meshEnabled, name, rogueEnabled, remoteSyslogEnabled, rtsaEnabled, vnaEnabled, wifiEnabled, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchOrgSites</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sites/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgSites(orgId, distinct = 'analytic_enabled', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgSites</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/sites/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSsoRoles(orgId, callback)</td>
    <td style="padding:15px">getOrgSsoRoles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssoroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgSsoRole(orgId, body, callback)</td>
    <td style="padding:15px">createOrgSsoRole</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssoroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgSsoRoles(orgId, ssoroleId, callback)</td>
    <td style="padding:15px">deleteOrgSsoRoles</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssoroles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSsoRole(orgId, ssoroleId, callback)</td>
    <td style="padding:15px">getOrgSsoRole</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssoroles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgSsoRole(orgId, ssoroleId, body, callback)</td>
    <td style="padding:15px">updateOrgSsoRole</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssoroles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSsos(orgId, callback)</td>
    <td style="padding:15px">getOrgSsos</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgSso(orgId, body, callback)</td>
    <td style="padding:15px">createOrgSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgSso(orgId, ssoId, callback)</td>
    <td style="padding:15px">deleteOrgSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSso(orgId, ssoId, callback)</td>
    <td style="padding:15px">getOrgSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgSso(orgId, ssoId, body, callback)</td>
    <td style="padding:15px">updateOrgSso</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSsoLatestFailures(orgId, ssoId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getOrgSsoLatestFailures</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos/{pathv2}/failures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgSsoSamlMetadata(orgId, ssoId, callback)</td>
    <td style="padding:15px">getOrgSsoSamlMetadata</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadOrgSsoSamlMetadata(orgId, ssoId, callback)</td>
    <td style="padding:15px">downloadOrgSsoSamlMetadata</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/ssos/{pathv2}/metadata.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgStats(orgId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getOrgStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgDevicesStats(orgId, page, limit, start, end, duration = '1d', type = 'ap', status = 'all', callback)</td>
    <td style="padding:15px">getOrgDevicesStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/stats/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdgesStats(orgId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getOrgMxEdgesStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/stats/mxedges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxEdgeStats(orgId, mxedgeId, callback)</td>
    <td style="padding:15px">getOrgMxEdgeStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/stats/mxedges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOrgMxTunnelsStats(distinct = 'mxcluster_id', mxclusterId, orgId, siteId, wxtunnelId, ap, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countOrgMxTunnelsStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/stats/tunnels/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgMxTunnelsStats(mxclusterId, orgId, siteId, wxtunnelId, ap, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getOrgMxTunnelsStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/stats/tunnels/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unsubscribeOrgAlarmsReports(orgId, callback)</td>
    <td style="padding:15px">unsubscribeOrgAlarmsReports</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscribeOrgAlarmsReports(orgId, callback)</td>
    <td style="padding:15px">subscribeOrgAlarmsReports</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgTemplates(orgId, callback)</td>
    <td style="padding:15px">getOrgTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgTemplate(orgId, body, callback)</td>
    <td style="padding:15px">createOrgTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgTemplate(orgId, templateId, callback)</td>
    <td style="padding:15px">deleteOrgTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgTemplate(orgId, templateId, callback)</td>
    <td style="padding:15px">getOrgTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgTemplate(orgId, templateId, body, callback)</td>
    <td style="padding:15px">updateOrgTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneOrgTemplate(orgId, templateId, body, callback)</td>
    <td style="padding:15px">cloneOrgTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/templates/{pathv2}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgTickets(orgId, start, end, callback)</td>
    <td style="padding:15px">getOrgTickets</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgTicket(orgId, body, callback)</td>
    <td style="padding:15px">createOrgTicket</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgTicket(orgId, ticketId, callback)</td>
    <td style="padding:15px">getOrgTicket</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/tickets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgTicket(orgId, ticketId, body, callback)</td>
    <td style="padding:15px">updateOrgTicket</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/tickets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOrgTicketComment(orgId, ticketId, body, callback)</td>
    <td style="padding:15px">addOrgTicketComment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/tickets/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWebhooks(orgId, callback)</td>
    <td style="padding:15px">getOrgWebhooks</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWebhook(orgId, body, callback)</td>
    <td style="padding:15px">createOrgWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWebhook(orgId, webhookId, callback)</td>
    <td style="padding:15px">deleteOrgWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/webhooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWebhook(orgId, webhookId, callback)</td>
    <td style="padding:15px">getOrgWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/webhooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWebhook(orgId, webhookId, body, callback)</td>
    <td style="padding:15px">updateOrgWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/webhooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWlans(orgId, callback)</td>
    <td style="padding:15px">getOrgWlans</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWlan(orgId, body, callback)</td>
    <td style="padding:15px">createOrgWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWlanDerived(orgId, resolve, callback)</td>
    <td style="padding:15px">getOrgWlanDerived</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/derived?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWlan(orgId, wlanId, callback)</td>
    <td style="padding:15px">deleteOrgWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWLAN(orgId, wlanId, callback)</td>
    <td style="padding:15px">getOrgWLAN</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWlan(orgId, wlanId, body, callback)</td>
    <td style="padding:15px">updateOrgWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWlanPortalImage(orgId, wlanId, callback)</td>
    <td style="padding:15px">deleteOrgWlanPortalImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/{pathv2}/portal_image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadOrgWlanPortalImage(orgId, wlanId, bodyFormData, callback)</td>
    <td style="padding:15px">uploadOrgWlanPortalImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/{pathv2}/portal_image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWlanPortalTemplate(orgId, wlanId, body, callback)</td>
    <td style="padding:15px">updateOrgWlanPortalTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wlans/{pathv2}/portal_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxRules(orgId, callback)</td>
    <td style="padding:15px">getOrgWxRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWxRule(orgId, body, callback)</td>
    <td style="padding:15px">createOrgWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxRulesDerived(orgId, callback)</td>
    <td style="padding:15px">getOrgWxRulesDerived</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxrules/derived?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWxRule(orgId, wxrulesId, callback)</td>
    <td style="padding:15px">deleteOrgWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxRule(orgId, wxrulesId, callback)</td>
    <td style="padding:15px">getOrgWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWxRule(orgId, wxrulesId, body, callback)</td>
    <td style="padding:15px">updateOrgWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxTags(orgId, callback)</td>
    <td style="padding:15px">getOrgWxTags</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWxTag(orgId, body, callback)</td>
    <td style="padding:15px">createOrgWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgApplicationList(orgId, callback)</td>
    <td style="padding:15px">getOrgApplicationList</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWxTag(orgId, wxtagId, callback)</td>
    <td style="padding:15px">deleteOrgWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxTag(orgId, wxtagId, callback)</td>
    <td style="padding:15px">getOrgWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWxTag(orgId, wxtagId, body, callback)</td>
    <td style="padding:15px">updateOrgWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgCurrentMatchingClientsOfAWxTag(orgId, wxtagId, callback)</td>
    <td style="padding:15px">getOrgCurrentMatchingClientsOfAWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtags/{pathv2}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxTunnels(orgId, callback)</td>
    <td style="padding:15px">getOrgWxTunnels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWxTunnel(orgId, body, callback)</td>
    <td style="padding:15px">createOrgWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWxTunnel(orgId, wxtunnelId, callback)</td>
    <td style="padding:15px">deleteOrgWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWxTunnel(orgId, wxtunnelId, callback)</td>
    <td style="padding:15px">getOrgWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWxTunnel(orgId, wxtunnelId, body, callback)</td>
    <td style="padding:15px">updateOrgWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/wxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSite(siteId, callback)</td>
    <td style="padding:15px">deleteSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteInfo(siteId, callback)</td>
    <td style="padding:15px">updateSiteInfo</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteInfo(siteId, body, callback)</td>
    <td style="padding:15px">updateSiteInfo</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testSiteWlanTwilioSetup(body, callback)</td>
    <td style="padding:15px">testSiteWlanTwilioSetup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/utils/test_twilio?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiAckSiteAlarms(siteId, body, callback)</td>
    <td style="padding:15px">multiAckSiteAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/ack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ackSiteAllAlarms(siteId, callback)</td>
    <td style="padding:15px">ackSiteAllAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/ack_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteAlarms(siteId, distinct = 'type', type, acked, severity, group, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAlarms(siteId, type, acked, severity, group, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiUnackSiteAlarms(siteId, body, callback)</td>
    <td style="padding:15px">multiUnackSiteAlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/unack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unackSiteAllArlarms(siteId, body, callback)</td>
    <td style="padding:15px">unackSiteAllArlarms</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/unack_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ackSiteAlarm(siteId, alarmId, body, callback)</td>
    <td style="padding:15px">ackSiteAlarm</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/{pathv2}/ack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unackSiteAlarm(siteId, alarmId, body, callback)</td>
    <td style="padding:15px">unackSiteAlarm</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/alarms/{pathv2}/unack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAnomalyEventsforDevice(siteId, deviceId, metric, callback)</td>
    <td style="padding:15px">getSiteAnomalyEventsforDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/anomaly/ap/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAnomalyEventsForClient(siteId, clientMac, metric, callback)</td>
    <td style="padding:15px">getSiteAnomalyEventsForClient</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/anomaly/client/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAnomalyEvents(siteId, metric, callback)</td>
    <td style="padding:15px">getSiteAnomalyEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/anomaly/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAssetFilters(siteId, callback)</td>
    <td style="padding:15px">getSiteAssetFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assetfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteAssetFilters(siteId, body, callback)</td>
    <td style="padding:15px">createSiteAssetFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assetfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteAssetFilter(siteId, assetfilterId, callback)</td>
    <td style="padding:15px">deleteSiteAssetFilter</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assetfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAssetFilter(siteId, assetfilterId, callback)</td>
    <td style="padding:15px">getSiteAssetFilter</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assetfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteAssetFilter(siteId, assetfilterId, body, callback)</td>
    <td style="padding:15px">updateSiteAssetFilter</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assetfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAssets(siteId, callback)</td>
    <td style="padding:15px">getSiteAssets</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteAsset(siteId, body, callback)</td>
    <td style="padding:15px">createSiteAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteAsset(siteId, assetId, callback)</td>
    <td style="padding:15px">deleteSiteAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAsset(siteId, assetId, callback)</td>
    <td style="padding:15px">getSiteAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteAsset(siteId, assetId, body, callback)</td>
    <td style="padding:15px">updateSiteAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/assets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteBeacons(siteId, callback)</td>
    <td style="padding:15px">getSiteBeacons</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/beacons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteBeacon(siteId, body, callback)</td>
    <td style="padding:15px">createSiteBeacon</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/beacons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteBeacons(siteId, beaconId, callback)</td>
    <td style="padding:15px">deleteSiteBeacons</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/beacons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteBeacon(siteId, beaconId, callback)</td>
    <td style="padding:15px">getSiteBeacon</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/beacons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteBeacons(siteId, beaconId, body, callback)</td>
    <td style="padding:15px">updateSiteBeacons</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/beacons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteByDistinctAttributesOfClients(siteId, distinct = 'ssid', ssid, ap, ip, vlan, hostname, os, model, device, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteByDistinctAttributesOfClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectSiteMultipleClients(siteId, body, callback)</td>
    <td style="padding:15px">disconnectSiteMultipleClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteByDistinctAttributesOfClientsEvents(siteId, distinct = 'type', type, reasonCode, ssid, ap, proto = 'b', band = '24', wlanId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteByDistinctAttributesOfClientsEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteClientEvents(siteId, type, reasonCode, ssid, ap, proto = 'b', band = '24', wlanId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteClientEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/events/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteClients(siteId, mac, hostname, device, os, model, ap, ssid, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteByDistinctAttributesOfClientSessions(siteId, distinct = 'ssid', ap, band, clientFamily, clientManufacture, clientModel, clientOs, ssid, wlanId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteByDistinctAttributesOfClientSessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/sessions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteClientSessions(siteId, ap, band, clientFamily, clientManufacture, clientModel, clientOs, ssid, wlanId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteClientSessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/sessions/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unauthorizeSiteMultipleClients(siteId, body, callback)</td>
    <td style="padding:15px">unauthorizeSiteMultipleClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/unauthorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectSiteClient(siteId, clientMac, callback)</td>
    <td style="padding:15px">disconnectSiteClient</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/{pathv2}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteEventsForClient(siteId, clientMac, type = 'b', proto = 'a', band, channel, wlanId, ssid, start, end, page, limit, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteEventsForClient</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/{pathv2}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unauthorizeSiteClient(siteId, clientMac, callback)</td>
    <td style="padding:15px">unauthorizeSiteClient</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/clients/{pathv2}/unauthorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDevices(siteId, type = 'ap', callback)</td>
    <td style="padding:15px">getSiteDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteDevice(siteId, body, callback)</td>
    <td style="padding:15px">createSiteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDeviceRadioChannels(siteId, countryCode, callback)</td>
    <td style="padding:15px">getSiteDeviceRadioChannels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/ap_channels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteDeviceConfigHistory(siteId, distinct, ap, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteDeviceConfigHistory</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/config_history/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteDeviceConfigHistory(siteId, ap, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteDeviceConfigHistory</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/config_history/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteDevices(siteId, distinct = 'model', hostname, model, mac, version, mxtunnelStatus, mxedgeId, lldpSystemName, lldpSystemDesc, lldpPortId, lldpMgmtAddr, mapId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteDeviceEvents(siteId, distinct = 'model', model, type, typeCode, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteDeviceEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteDeviceEvents(siteId, model, type, typeCode, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteDeviceEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/events/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportSiteDeviceInformation(siteId, callback)</td>
    <td style="padding:15px">exportSiteDeviceInformation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteDeviceLastConfig(siteId, distinct = 'ap', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteDeviceLastConfig</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/last_config/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteDeviceLastConfigs(ap, version, name, siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteDeviceLastConfigs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/last_config/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reprovisionSiteAllAps(siteId, callback)</td>
    <td style="padding:15px">reprovisionSiteAllAps</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/reprovision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetSiteAllApsToUseRrm(siteId, body, callback)</td>
    <td style="padding:15px">resetSiteAllApsToUseRrm</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/reset_radio_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiRestartSiteDevices(siteId, body, callback)</td>
    <td style="padding:15px">multiRestartSiteDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteDevices(hostname, siteId, type = 'ap', model, mac, version, ip, mxtunnelStatus = 'up', mxedgeId, lldpSystemName, lldpSystemDesc, lldpPortId, lldpMgmtAddr, sort = 'timestamp', descSort = 'timestamp', stats, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiUpgradeSiteDevices(siteId, body, callback)</td>
    <td style="padding:15px">multiUpgradeSiteDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAvailableDeviceVersions(siteId, callback)</td>
    <td style="padding:15px">getSiteAvailableDeviceVersions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zeroizeSiteFipsAllAps(siteId, body, callback)</td>
    <td style="padding:15px">zeroizeSiteFipsAllAps</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/zerioze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDeviceConfigCmd(siteId, deviceId, callback)</td>
    <td style="padding:15px">getSiteDeviceConfigCmd</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/config_cmd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">deleteSiteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">getSiteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteDevice(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">updateSiteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDeviceIotPort(siteId, deviceId, callback)</td>
    <td style="padding:15px">getSiteDeviceIotPort</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/iot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSiteDeviceIotPort(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">setSiteDeviceIotPort</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/iot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSiteLocateDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">startSiteLocateDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/locate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartSiteDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">restartSiteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadSiteDeviceSupportFile(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">uploadSiteDeviceSupportFile</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/support?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopSiteLocateDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">stopSiteLocateDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/unlocate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeSiteDevice(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">upgradeSiteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteVirtualChassis(siteId, deviceId, callback)</td>
    <td style="padding:15px">deleteSiteVirtualChassis</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/vc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteVirtualChassis(siteId, deviceId, callback)</td>
    <td style="padding:15px">getSiteVirtualChassis</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/vc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteVirtualChassis(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">createSiteVirtualChassis</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/vc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSiteVirtualChassisMember(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">updateSiteVirtualChassisMember</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/vc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteDeviceImage(siteId, deviceId, imageName, callback)</td>
    <td style="padding:15px">deleteSiteDeviceImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSiteDeviceImage(siteId, deviceId, imageName, bodyFormData, callback)</td>
    <td style="padding:15px">addSiteDeviceImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pollSiteSwitchStats(siteId, deviceId, callback)</td>
    <td style="padding:15px">pollSiteSwitchStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/poll_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteEvpnTopology(siteId, body, callback)</td>
    <td style="padding:15px">createSiteEvpnTopology</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/build_evpn_topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">arpFromDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">arpFromDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portsBounceFromSwitch(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">portsBounceFromSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/bounce_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cableTestFromSwitch(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">cableTestFromSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/cable_test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingFromDevice(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">pingFromDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tracerouteFromDevice(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">tracerouteFromDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/traceroute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAllLearnedMacsFromPortOnSwitch(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">clearAllLearnedMacsFromPortOnSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/clear_macs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearBpduErrosFromPortsOnSwitch(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">clearBpduErrosFromPortsOnSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/devices/{pathv2}/clear_bpdu_error?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRoamingEvents(siteId, type = 'success', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteRoamingEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/events/fast_roam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteInterferenceEvents(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteInterferenceEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/events/interference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteSystemEvents(siteId, distinct = 'type', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteSystemEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/events/system/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteSystemEvents(siteId, type, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteSystemEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/events/system/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAllGuestAuthorizations(siteId, callback)</td>
    <td style="padding:15px">getSiteAllGuestAuthorizations</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/guests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteGuestAuthorizations(siteId, distinct = 'auth_method', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteGuestAuthorizations</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/guests/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteGuestAuthorization(siteId, wlanId, authMethod, ssid, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteGuestAuthorization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/guests/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteGuestAuthorization(siteId, guestMac, callback)</td>
    <td style="padding:15px">deleteSiteGuestAuthorization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/guests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteGuestAuthorization(siteId, guestMac, callback)</td>
    <td style="padding:15px">getSiteGuestAuthorization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/guests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteGuestAuthorization(siteId, guestMac, body, callback)</td>
    <td style="padding:15px">updateSiteGuestAuthorization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/guests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteInsightMetricsForDevice(siteId, deviceId, metric, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteInsightMetricsForDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/insights/ap/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteInsightMetricsForClient(siteId, clientMac, metric, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteInsightMetricsForClient</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/insights/client/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRogueAPs(siteId, type = 'honeypot', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteRogueAPs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/insights/rogues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRogueClients(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteRogueClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/insights/rogues/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteInsightMetrics(siteId, metric, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteInsightMetrics</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/insights/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteLicenseUsage(siteId, callback)</td>
    <td style="padding:15px">getSiteLicenseUsage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/licenses/usages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteBeamCoverageOverview(siteId, mapId, type = 'sdkclient', duration = '1d', resolution, clientType, start, end, callback)</td>
    <td style="padding:15px">getSiteBeamCoverageOverview</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/coverage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMachineLearningCurrentStat(siteId, mapId, callback)</td>
    <td style="padding:15px">getSiteMachineLearningCurrentStat</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDefaultPlfForModels(siteId, callback)</td>
    <td style="padding:15px">getSiteDefaultPlfForModels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearSiteMlOverwriteForDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">clearSiteMlOverwriteForDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/device/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overwriteSiteMlForDevice(siteId, deviceId, body, callback)</td>
    <td style="padding:15px">overwriteSiteMlForDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/device/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearSiteMlOverwriteForMap(siteId, mapId, callback)</td>
    <td style="padding:15px">clearSiteMlOverwriteForMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/map/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overwriteSiteMlForMap(siteId, mapId, body, callback)</td>
    <td style="padding:15px">overwriteSiteMlForMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/map/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetSiteMlStatsByMap(siteId, mapId, callback)</td>
    <td style="padding:15px">resetSiteMlStatsByMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/reset/map/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMachineLearningEvents(siteId, deviceId, mapIp, clientType, duration, start, end, interval, callback)</td>
    <td style="padding:15px">getSiteMachineLearningEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/location/ml/updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMaps(siteId, callback)</td>
    <td style="padding:15px">getSiteMaps</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteMap(siteId, body, callback)</td>
    <td style="padding:15px">createSiteMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteMap(siteId, mapId, callback)</td>
    <td style="padding:15px">deleteSiteMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMap(siteId, mapId, callback)</td>
    <td style="padding:15px">getSiteMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteMap(siteId, mapId, body, callback)</td>
    <td style="padding:15px">updateSiteMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteMapImage(siteId, mapId, callback)</td>
    <td style="padding:15px">deleteSiteMapImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}/image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSiteMapImage(siteId, mapId, bodyFormData, callback)</td>
    <td style="padding:15px">addSiteMapImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}/image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSiteMapImage(siteId, mapId, body, callback)</td>
    <td style="padding:15px">replaceSiteMapImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}/replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importSiteWayfinding(siteId, mapId, body, callback)</td>
    <td style="padding:15px">importSiteWayfinding</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/maps/{pathv2}/wayfinding/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitePacketCaptures(siteId, page, limit, start, end, duration = '1d', clientMac, callback)</td>
    <td style="padding:15px">getSitePacketCaptures</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/pcaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopSitePacketCapture(siteId, callback)</td>
    <td style="padding:15px">stopSitePacketCapture</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/pcaps/capture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCapturingStatus(siteId, callback)</td>
    <td style="padding:15px">getSiteCapturingStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/pcaps/capture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSiteRadiotapPacketCapture(siteId, body, callback)</td>
    <td style="padding:15px">startSiteRadiotapPacketCapture</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/pcaps/capture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteRfdiagRecording(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteSiteRfdiagRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSiteRecording(siteId, body, callback)</td>
    <td style="padding:15px">startSiteRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteRfdiagRecording(siteId, rfdiagId, callback)</td>
    <td style="padding:15px">deleteSiteRfdiagRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRfdiagRecording(siteId, rfdiagId, callback)</td>
    <td style="padding:15px">getSiteRfdiagRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteRfdiagRecording(siteId, rfdiagId, body, callback)</td>
    <td style="padding:15px">updateSiteRfdiagRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadSiteRfdiagRecording(siteId, rfdiagId, callback)</td>
    <td style="padding:15px">downloadSiteRfdiagRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags/{pathv2}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopSiteRfdiagRecording(siteId, rfdiagId, callback)</td>
    <td style="padding:15px">stopSiteRfdiagRecording</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rfdiags/{pathv2}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteRogueEvents(siteId, distinct = 'bssid', type = 'honeypot', ssid, bssid, apMac, channel, seenOnLan, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteRogueEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rogues/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteRogueEvents(siteId, type = 'honeypot', ssid, bssid, apMac, channel, seenOnLan, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteRogueEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rogues/events/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRogueAP(siteId, rogueBssid, callback)</td>
    <td style="padding:15px">getSiteRogueAP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rogues/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deauthSiteClientsConnectedToARogue(siteId, rogueBssid, callback)</td>
    <td style="padding:15px">deauthSiteClientsConnectedToARogue</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rogues/{pathv2}/deauth_clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCurrentChannelPlanning(siteId, callback)</td>
    <td style="padding:15px">getSiteCurrentChannelPlanning</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rrm/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCurrentRrmConsiderationsForAnApOnASpecificBand(siteId, deviceId, band = '24', callback)</td>
    <td style="padding:15px">getSiteCurrentRrmConsiderationsForAnApOnASpecificBand</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rrm/current/devices/{pathv2}/band/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRrmEvents(siteId, band = '24', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteRrmEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rrm/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">optimizeSiteRrm(siteId, body, callback)</td>
    <td style="padding:15px">optimizeSiteRrm</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rrm/optimize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRssiZones(siteId, callback)</td>
    <td style="padding:15px">getSiteRssiZones</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rssizones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteRssiZone(siteId, body, callback)</td>
    <td style="padding:15px">createSiteRssiZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rssizones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteRssiZone(siteId, rssizoneId, callback)</td>
    <td style="padding:15px">deleteSiteRssiZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rssizones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteRssiZone(siteId, rssizoneId, callback)</td>
    <td style="padding:15px">getSiteRssiZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rssizones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteRssiZone(siteId, rssizoneId, body, callback)</td>
    <td style="padding:15px">updateSiteRssiZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/rssizones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSetting(siteId, callback)</td>
    <td style="padding:15px">getSiteSetting</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteSettings(siteId, body, callback)</td>
    <td style="padding:15px">updateSiteSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteClientsBlocklist(siteId, callback)</td>
    <td style="padding:15px">deleteSiteClientsBlocklist</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting/blacklist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteClientsBlocklist(siteId, body, callback)</td>
    <td style="padding:15px">createSiteClientsBlocklist</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting/blacklist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteWatchedStations(siteId, callback)</td>
    <td style="padding:15px">deleteSiteWatchedStations</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting/watched_station?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteWatchedStations(siteId, body, callback)</td>
    <td style="padding:15px">createSiteWatchedStations</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting/watched_station?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteClientsAllowlist(siteId, callback)</td>
    <td style="padding:15px">deleteSiteClientsAllowlist</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting/whitelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteClientsAllowlist(siteId, body, callback)</td>
    <td style="padding:15px">createSiteClientsAllowlist</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/setting/whitelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteByDistringAttributesOfSkyatpEvents(siteId, distinct = 'type', type, mac, deviceMac, threatLevel, ip, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteByDistringAttributesOfSkyatpEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/skyatp/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteSkyatpEvents(siteId, type, mac, deviceMac, threatLevel, ip, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteSkyatpEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/skyatp/events/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteStats(siteId, callback)</td>
    <td style="padding:15px">getSiteStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAssetsStats(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteAssetsStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAssetStats(siteId, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteAssetStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/assets/asset_id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteBeaconsStats(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteBeaconsStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/beacons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteClientsStats(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteClientsStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteClientStats(siteId, clientMac, callback)</td>
    <td style="padding:15px">getSiteClientStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/clients/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDevicesStats(siteId, page, limit, start, end, duration = '1d', type = 'ap', status = 'all', callback)</td>
    <td style="padding:15px">getSiteDevicesStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDeviceStats(siteId, deviceId, callback)</td>
    <td style="padding:15px">getSiteDeviceStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAllClientsStatsOnDevice(siteId, deviceId, callback)</td>
    <td style="padding:15px">getSiteAllClientsStatsOnDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/devices/{pathv2}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDiscoveredAsset(siteId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteDiscoveredAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/discovered_assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteDiscoveredSwitchesMetrics(siteId, scope = 'site', type = 'inactive_wired_vlans', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteDiscoveredSwitchesMetrics</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/discovered_switch_metrics/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteDiscoveredSwitches(siteId, distinct = 'system_name', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteDiscoveredSwitches</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/discovered_switches/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDiscoveredSwitchesMetrics(siteId, threshold, systemName, callback)</td>
    <td style="padding:15px">getSiteDiscoveredSwitchesMetrics</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/discovered_switches/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDiscoveredSwitches(siteId, systemName, mgmtAddr, model, version, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteDiscoveredSwitches</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/discovered_switches/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteClientsStatsByMap(siteId, mapId, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSiteClientsStatsByMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/maps/{pathv2}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDiscoveredAssetByMap(siteId, mapId, callback)</td>
    <td style="padding:15px">getSiteDiscoveredAssetByMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/maps/{pathv2}/discovered_assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSdkStatsByMap(siteId, mapId, callback)</td>
    <td style="padding:15px">getSiteSdkStatsByMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/maps/{pathv2}/sdkclients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteUnconnectedClientStats(siteId, mapId, callback)</td>
    <td style="padding:15px">getSiteUnconnectedClientStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/maps/{pathv2}/unconnected_clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMxEdgesStats(siteId, callback)</td>
    <td style="padding:15px">getSiteMxEdgesStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/mxedges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMxEdgeStats(siteId, mxedgeId, callback)</td>
    <td style="padding:15px">getSiteMxEdgeStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/mxedges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteByDisctinctAttributesOPorts(siteId, distinct = 'port_id', fullDuplex, mac, neighborMac, neighborPortDesc, neighborSystemName, poeDisabled, poeMode, poeOn, portId, portMac, powerDraw, txPkts, rxPkts, rxBytes, txBps, rxBps, txMcastPkts, txBcastPkts, rxMcastPkts, rxBcastPkts, speed, stpState = 'forwarding', stpRole = 'designated', authState = 'init', up, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteByDisctinctAttributesOfSwitchPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/ports/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSitePorts(siteId, fullDuplex, mac, neighborMac, neighborPortDesc, neighborSystemName, poeDisabled, poeMode, poeOn, portId, portMac, powerDraw, txPkts, rxPkts, rxBytes, txBps, rxBps, txErrors, rxErrors, txMcastPkts, txBcastPkts, rxMcastPkts, rxBcastPkts, speed, up, stpState = 'forwarding', stpRole = 'designated', authState = 'init', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteSwitchPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/ports/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSdkStats(siteId, sdkclientId, callback)</td>
    <td style="padding:15px">getSiteSdkStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/sdkclients/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteByDisctinctAttributesOfSwitchPorts(siteId, distinct = 'port_id', fullDuplex, mac, neighborMac, neighborPortDesc, neighborSystemName, poeDisabled, poeMode, poeOn, portId, portMac, powerDraw, txPkts, rxPkts, rxBytes, txBps, rxBps, txMcastPkts, txBcastPkts, rxMcastPkts, rxBcastPkts, speed, stpState = 'forwarding', stpRole = 'designated', authState = 'init', up, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteByDisctinctAttributesOfSwitchPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/switch_ports/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteSwitchPorts(siteId, fullDuplex, mac, neighborMac, neighborPortDesc, neighborSystemName, poeDisabled, poeMode, poeOn, portId, portMac, powerDraw, txPkts, rxPkts, rxBytes, txBps, rxBps, txMcastPkts, txBcastPkts, rxMcastPkts, rxBcastPkts, speed, stpState = 'forwarding', stpRole = 'designated', authState = 'init', up, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteSwitchPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/switch_ports/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxRulesUsage(siteId, callback)</td>
    <td style="padding:15px">getSiteWxRulesUsage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/wxrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteZonesStats(siteId, mapId, callback)</td>
    <td style="padding:15px">getSiteZonesStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteZoneStats(siteId, zoneType = 'zones', zoneId, callback)</td>
    <td style="padding:15px">getSiteZoneStats</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/stats/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unsubscribeSite(siteId, callback)</td>
    <td style="padding:15px">UnsubscribeSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscribeSite(siteId, callback)</td>
    <td style="padding:15px">SubscribeSite</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCurdSettings(siteId, callback)</td>
    <td style="padding:15px">getSiteCurdSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/uisettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteCurdSettings(siteId, body, callback)</td>
    <td style="padding:15px">createSiteCurdSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/uisettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteDerivedCurdSetting(siteId, callback)</td>
    <td style="padding:15px">getSiteDerivedCurdSetting</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/uisettings/derived?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteCurdSetting(siteId, uisettingId, callback)</td>
    <td style="padding:15px">deleteSiteCurdSetting</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/uisettings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCurdSetting(siteId, uisettingId, callback)</td>
    <td style="padding:15px">getSiteCurdSetting</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/uisettings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteCurdSetting(siteId, uisettingId, body, callback)</td>
    <td style="padding:15px">updateSiteCurdSetting</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/uisettings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteVBeacons(siteId, callback)</td>
    <td style="padding:15px">getSiteVBeacons</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/vbeacons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteVBeacon(siteId, body, callback)</td>
    <td style="padding:15px">createSiteVBeacon</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/vbeacons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteVBeacon(siteId, vbeaconId, callback)</td>
    <td style="padding:15px">deleteSiteVBeacon</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/vbeacons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteVBeacon(siteId, vbeaconId, callback)</td>
    <td style="padding:15px">getSiteVBeacon</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/vbeacons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteVBeacon(siteId, vbeaconId, body, callback)</td>
    <td style="padding:15px">updateSiteVBeacon</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/vbeacons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWebhooks(siteId, callback)</td>
    <td style="padding:15px">getSiteWebhooks</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteWebhook(siteId, body, callback)</td>
    <td style="padding:15px">createSiteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteWebhook(siteId, webhookId, callback)</td>
    <td style="padding:15px">deleteSiteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/webhooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWebhook(siteId, webhookId, callback)</td>
    <td style="padding:15px">getSiteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/webhooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteWebhook(siteId, webhookId, body, callback)</td>
    <td style="padding:15px">updateSiteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/webhooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingSiteWebhook(siteId, webhookId, callback)</td>
    <td style="padding:15px">pingSiteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/webhooks/{pathv2}/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteWiredClients(siteId, distinct = 'port_id', mac, deviceMac, portId, vlan, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteWiredClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wired_clients/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteWiredClients(siteId, mac, deviceMac, portId, vlan, page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteWiredClients</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wired_clients/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWlans(siteId, callback)</td>
    <td style="padding:15px">getSiteWlans</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteWlan(siteId, body, callback)</td>
    <td style="padding:15px">createSiteWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWlanDerived(siteId, resolve, callback)</td>
    <td style="padding:15px">getSiteWlanDerived</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans/derived?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteWlan(siteId, wlanId, callback)</td>
    <td style="padding:15px">deleteSiteWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWlan(siteId, wlanId, callback)</td>
    <td style="padding:15px">getSiteWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteWlan(siteId, wlanId, body, callback)</td>
    <td style="padding:15px">updateSiteWlan</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadSiteWlanPortalImage(siteId, wlanId, body, callback)</td>
    <td style="padding:15px">uploadSiteWlanPortalImage</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans/{pathv2}/portal_image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteWlanPortalTemplate(siteId, wlanId, body, callback)</td>
    <td style="padding:15px">updateSiteWlanPortalTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wlans/{pathv2}/portal_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxRules(siteId, callback)</td>
    <td style="padding:15px">getSiteWxRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteWxRule(siteId, body, callback)</td>
    <td style="padding:15px">createSiteWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxRulesDerived(siteId, callback)</td>
    <td style="padding:15px">getSiteWxRulesDerived</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxrules/derived?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteWxRule(siteId, wxrulesId, callback)</td>
    <td style="padding:15px">deleteSiteWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxRule(siteId, wxrulesId, callback)</td>
    <td style="padding:15px">getSiteWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteWxRule(siteId, wxrulesId, body, callback)</td>
    <td style="padding:15px">updateSiteWxRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxTags(siteId, callback)</td>
    <td style="padding:15px">getSiteWxTags</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteWxTag(siteId, body, callback)</td>
    <td style="padding:15px">createSiteWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteApplicationList(siteId, callback)</td>
    <td style="padding:15px">getSiteApplicationList</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteWxTag(siteId, wxtagId, callback)</td>
    <td style="padding:15px">deleteSiteWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxTag(siteId, wxtagId, callback)</td>
    <td style="padding:15px">getSiteWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteWxTag(siteId, wxtagId, body, callback)</td>
    <td style="padding:15px">updateSiteWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCurrentMatchingClientsOfAWxTag(siteId, wxtagId, callback)</td>
    <td style="padding:15px">getSiteCurrentMatchingClientsOfAWxTag</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtags/{pathv2}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxTunnels(siteId, callback)</td>
    <td style="padding:15px">getSiteWxTunnels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteWxTunnel(siteId, body, callback)</td>
    <td style="padding:15px">createSiteWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteWxTunnel(siteId, wxtunnelId, callback)</td>
    <td style="padding:15px">deleteSiteWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteWxTunnel(siteId, wxtunnelId, callback)</td>
    <td style="padding:15px">getSiteWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteWxTunnel(siteId, wxtunnelId, body, callback)</td>
    <td style="padding:15px">updateSiteWxTunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/wxtunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteZones(siteId, callback)</td>
    <td style="padding:15px">getSiteZones</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteZone(siteId, body, callback)</td>
    <td style="padding:15px">createSiteZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteZone(siteId, zoneId, callback)</td>
    <td style="padding:15px">deleteSiteZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteZone(siteId, zoneId, callback)</td>
    <td style="padding:15px">getSiteZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteZone(siteId, zoneId, body, callback)</td>
    <td style="padding:15px">updateSiteZone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSiteZoneSessions(siteId, zoneType = 'zones', distinct = 'user_type', userType = 'client', user, scopeId, scope = 'site', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">countSiteZoneSessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/{pathv2}/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSiteZoneSessions(siteId, zoneType = 'zones', userType = 'client', user, scopeId, scope = 'site', page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">searchSiteZoneSessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/{pathv2}/visits/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplications(callback)</td>
    <td style="padding:15px">getApplications</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApModels(callback)</td>
    <td style="padding:15px">getApModels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/device_models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteLanguages(callback)</td>
    <td style="padding:15px">getSiteLanguages</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/languages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApLedDefinition(callback)</td>
    <td style="padding:15px">getApLedDefinition</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/ap_led_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientEventsDefinitions(callback)</td>
    <td style="padding:15px">getClientEventsDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/client_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountryCodes(callback)</td>
    <td style="padding:15px">getCountryCodes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/countries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemEventsDefinitions(callback)</td>
    <td style="padding:15px">getSystemEventsDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/system_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteAvailableInsightMetrics(callback)</td>
    <td style="padding:15px">getSiteAvailableInsightMetrics</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/insight_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmDefinitions(callback)</td>
    <td style="padding:15px">getAlarmDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/alarm_defs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApChannels(countryCode, callback)</td>
    <td style="padding:15px">getApChannels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/ap_channels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMxEdgeModels(callback)</td>
    <td style="padding:15px">getMxEdgeModels</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/const/mxedge_models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSubscriptions(callback)</td>
    <td style="padding:15px">getAlarmSubscriptions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSelf(callback)</td>
    <td style="padding:15px">getSelf</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSelf(body, callback)</td>
    <td style="padding:15px">updateSelf</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSelf(callback)</td>
    <td style="padding:15px">deleteSelf</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSelfEmail(body, callback)</td>
    <td style="padding:15px">updateSelfEmail</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifySelfEmail(token, callback)</td>
    <td style="padding:15px">verifySelfEmail</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/update/verify/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateQrCodeForVerification(by = 'qrcode', callback)</td>
    <td style="padding:15px">generateQrCodeForVerification</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/two_factor/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyTwoFactor(body, callback)</td>
    <td style="padding:15px">verifyTwoFactor</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/two_factor/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recoverPassword(body, callback)</td>
    <td style="padding:15px">recoverPassword</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/recover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyRecoverPasssword(token, callback)</td>
    <td style="padding:15px">verifyRecoverPasssword</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/recover/verify/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSelfAuditLogs(page, limit, start, end, duration = '1d', callback)</td>
    <td style="padding:15px">getSelfAuditLogs</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(body, callback)</td>
    <td style="padding:15px">login</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">logout</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lookup(body, callback)</td>
    <td style="padding:15px">lookup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/login/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">twoFactor(body, callback)</td>
    <td style="padding:15px">twoFactor</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/login/two_factor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOAuth2UrlForLinking(provider, forward, callback)</td>
    <td style="padding:15px">getOAuth2UrlForLinking</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/oauth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkOAuth2MistAccount(provider, body, callback)</td>
    <td style="padding:15px">linkOAuth2MistAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/oauth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOAuth2AuthorizationUrlForLogin(provider, forward, callback)</td>
    <td style="padding:15px">getOAuth2AuthorizationUrlForLogin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/login/oauth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginOAuth2(provider, body, callback)</td>
    <td style="padding:15px">loginOAuth2</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/login/oauth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlinkOAuth2Provider(provider, callback)</td>
    <td style="padding:15px">unlinkOAuth2Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/login/oauth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiTokens(callback)</td>
    <td style="padding:15px">getApiTokens</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiToken(callback)</td>
    <td style="padding:15px">createApiToken</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiToken(apitokenId, callback)</td>
    <td style="padding:15px">deleteApiToken</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/self/apitokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerNewAdmin(body, callback)</td>
    <td style="padding:15px">registerNewAdmin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyRegistration(token, callback)</td>
    <td style="padding:15px">verifyRegistration</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/register/verify/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteApplications(siteId, callback)</td>
    <td style="padding:15px">Get Site Applications</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sites/{pathv1}/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgNetworks(orgId, callback)</td>
    <td style="padding:15px">getOrgNetworks</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgNetwork(orgId, body, callback)</td>
    <td style="padding:15px">createOrgNetwork</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgNetwork(orgId, networkId, body, callback)</td>
    <td style="padding:15px">updateOrgNetwork</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgNetwork(orgId, networkId, callback)</td>
    <td style="padding:15px">deleteOrgNetwork</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV1OrgsOrgIdNetworksNetworkId(orgId, networkId, callback)</td>
    <td style="padding:15px">getOrgNetwork</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgGatewayTemplates(orgId, callback)</td>
    <td style="padding:15px">getOrgGatewayTemplates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/gatewaytemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgGatewayTemplate(orgId, body, callback)</td>
    <td style="padding:15px">createOrgGatewayTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/gatewaytemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgGatewayTemplate(orgId, gatewaytemplateId, body, callback)</td>
    <td style="padding:15px">updateOrgGatewayTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/gatewaytemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgGatewayTemplate(orgId, gatewaytemplateId, callback)</td>
    <td style="padding:15px">deleteOrgGatewayTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/gatewaytemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgGatewayTemplate(orgId, gatewaytemplateId, callback)</td>
    <td style="padding:15px">getOrgGatewayTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/gatewaytemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrg128TRegistrationCommands(orgId, callback)</td>
    <td style="padding:15px">getOrg128TRegistrationCommands</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/128routers/register_cmd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgServices(orgId, callback)</td>
    <td style="padding:15px">getOrgServices</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgService(orgId, body, callback)</td>
    <td style="padding:15px">createOrgService</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/orgs/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
