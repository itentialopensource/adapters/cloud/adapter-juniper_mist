# Juniper Mist

Vendor: Juniper
Homepage: https://www.juniper.net/

Product: Juniper Mist
Product Page: https://www.mist.com/

## Introduction
We classify Juniper Mist into the Cloud domain as Juniper Mist controls/orchestrates actions within the Cloud.

## Why Integrate
The Juniper Mist adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Mist to performs instantiation, management, and scalability of a cloud-based wired and wireless infrastructure.

With this adapter you have the ability to perform operations with Juniper Mist such as:

- Organizations
- Networks
- Sites
- Tunnels
- Devices
- Rules

## Additional Product Documentation
The [API documents for Juniper Mist](https://doc.mist-lab.fr/)